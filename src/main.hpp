#ifndef MAIN_H_
#define MAIN_H_

// Include necessary homekit libraries
#include <homekit/types.h>
#include <homekit/characteristics.h>
#include <homekit/homekit.h>

// Include the definition of CodeEntry from bell.hpp
#include "bell.hpp"

// Pins lock
const int PIN_RELAY_ON = 13;
const int PIN_RELAY_OFF = 12;

const int PIN_LED_UNLOCKED = 2;  // Pin for the unlocked LED
const int PIN_LED_LOCKED = 0;    // Pin for the locked LED

// Pin for the bell
const int BELL_PIN = 14;


// Define constants for button press types
#define SHORT_RING 0
#define LONG_RING 1

// External declarations for homekit characteristics
extern homekit_characteristic_t cha_lock_current_state;
extern homekit_characteristic_t cha_lock_target_state;

// External declarations for button press pattern and its size
extern const int pattern[];
extern const int pattern_size;

// External declaration for the lock status
extern bool unlocked;

#endif
