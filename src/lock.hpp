#ifndef LOCK_H_
#define LOCK_H_

#include <homekit/types.h>

/**
 * @brief Set up the lock by configuring pins.
 * 
 * This function initializes the lock by setting up the required pins for the relay and LEDs.
 */
void setup_lock();

/**
 * @brief Set the lock state based on the provided value.
 * 
 * @param value The new lock state value.
 * 
 * This function sets the lock state based on the provided value.
 */
void set_lock(const homekit_value_t value);

#endif
