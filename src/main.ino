#include <Arduino.h>
#include <arduino_homekit_server.h>
#include "wifi_info.h"
#include "main.hpp"
#include "lock.hpp"
#include "bell.hpp"

// Define a macro for logging
#define LOG_D(fmt, ...)   printf_P(PSTR(fmt "\n") , ##__VA_ARGS__)

// Constants
#define PIN_LED LED_BUILTIN

// Define the pattern for button presses
const int pattern[] = {SHORT_RING, SHORT_RING};
const int pattern_size = sizeof(pattern) / sizeof(pattern[0]);

// External declaration of the homekit server configuration
extern homekit_server_config_t config;

// Variable to store the next heap check time
static uint32_t next_heap_millis = 0;

// Function prototypes
void my_homekit_setup();
void my_homekit_loop();

// Setup function
void setup() {
    // Initialize serial communication
    Serial.begin(115200);
    while (!Serial);
    Serial.println("Starting....");
  
    // Set pin modes
    digitalWrite(PIN_LED, HIGH);
    setup_bell();
    setup_lock();  
    wifi_connect();
    // homekit_storage_reset();
    my_homekit_setup();
}

// Main loop function
void loop() {
    loop_input();
    my_homekit_loop();
    delay(10);
}

// HomeKit setup function
void my_homekit_setup() {
    // Set the lock target state setter function
    cha_lock_target_state.setter = set_lock;
    // Setup the homekit server
    arduino_homekit_setup(&config);  
}

// HomeKit loop function
void my_homekit_loop() {
    // Run the homekit server loop
    arduino_homekit_loop();
  
    // Check the heap every 30 seconds
    const uint32_t t = millis();
    if (t > next_heap_millis) {
        // Update the next heap check time
        next_heap_millis = t + 30 * 1000;
        // Print heap info
        LOG_D("Free heap: %d, HomeKit clients: %d",
            ESP.getFreeHeap(), arduino_homekit_connected_clients_count());
    }
}