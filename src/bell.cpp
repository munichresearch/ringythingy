#include <Arduino.h>
#include <HardwareSerial.h>
#include <LinkedList.h>
#include <homekit/homekit.h>
#include <homekit/characteristics.h>
#include <string.h>
#include "lock.hpp"
#include "bell.hpp"
#include "main.hpp"

// External variables
extern homekit_characteristic_t cha_lock_current_state;
extern homekit_characteristic_t cha_lock_target_state;
extern const int pattern[];

// Debounce variables
static unsigned long lastInputTime = 0;            // Time of the last bell input
static int lastSteadyState = HIGH;                 // Last stable state of the bell
static int lastFlickerableState = HIGH;            // Last flickerable state of the bell
static unsigned long lastDebounceTime = 0;         // Time of last debounce
static unsigned long pressStartTime = 0;           // Time when the bell press started
static unsigned long timeSinceRelayOn = 0;         // Time when the last bell press ended
LinkedList<int> currentPattern;                    // Current sequence of bell presses
BellState currentState = IDLE;                     // Current state of the system

/**
 * @brief Set up the bell by initializing variables and configuring pins.
 */
void setup_bell() {
  pinMode(BELL_PIN, INPUT_PULLUP);
  currentPattern = LinkedList<int>();
  lastDebounceTime = millis();
  lastSteadyState = digitalRead(BELL_PIN);
}

/**
 * @brief Handle the bell ring event.
 */
void handleBellRing() {
  pressStartTime = millis(); // Record the time when the bell ring started
  currentState = WAIT_FOR_STOP;
}

/**
 * @brief Handle the bell off event.
 */
void handleBellOff() {
  lastInputTime = millis();
  unsigned long ringDuration = millis() - pressStartTime; // Calculate press duration
  detectPattern(ringDuration); // Detect pattern based on press duration
  currentState = WAIT_FOR_TIMEOUT;
}

/**
 * @brief Detect the bell ring pattern.
 * 
 * @param ringDuration The duration of the bell ring.
 */
void detectPattern(unsigned long ringDuration) {
  if (10 <= ringDuration && ringDuration < (LONG_PRESS_DURATION - TOLERANCE_PRESS)) {
    currentPattern.add(SHORT_RING);
  } else if ((LONG_PRESS_DURATION - TOLERANCE_PRESS) <=  ringDuration && ringDuration <=  (LONG_PRESS_DURATION + 500)) {
    currentPattern.add(LONG_RING);
  }
  printCurrentPattern(); // Print the current pattern
}

/**
 * @brief Print the current bell ring pattern.
 */
void printCurrentPattern() {
  if (currentPattern.size() > 0) {
    Serial.print("Current Pattern: ");
    for (int i = 0; i < currentPattern.size(); ++i) {
      Serial.print(currentPattern.get(i)); 
      Serial.print(" ");
    }
    Serial.println();
  } else {
    Serial.println("Current Pattern: ");
  }
}

/**
 * @brief Validate the current bell ring pattern.
 * 
 * @return True if the pattern is valid, false otherwise.
 */
bool validatePattern() {
  int currentPatternSize = currentPattern.size();
  bool patternValid = true;
  if (currentPatternSize == pattern_size) {
    for (int i = 0; i < currentPatternSize; ++i) {
      if(currentPattern.get(i) != pattern[i]) {
        patternValid = false;
        break;
      }  
    }
  } else {
    patternValid = false;
  }
  Serial.print("Pattern is ");
  Serial.println(patternValid ? "valid" : "invalid");
  return patternValid;
}

/**
 * @brief Convert the ring duration to a string representation.
 * 
 * @param state The state of the bell.
 * @return String representation of the bell state.
 */
String stateToString(BellState state) {
  switch(state) {
    case IDLE:
      return "IDLE";
    case WAIT_FOR_STOP:
      return "WAIT_FOR_STOP";
    case WAIT_FOR_TIMEOUT:
      return "WAIT_FOR_TIMEOUT";
    case RELAY_ON:
      return "RELAY_ON";
    default:
      return "UNKNOWN_STATE";
  }
}

/**
 * @brief Main loop function to handle bell input and state transitions.
 */
void loop_input() {
  // Read the state of the bell with debounce
  int bellState = digitalRead(BELL_PIN);
  
  // Update debounce time if bell state changes
  if (bellState != lastFlickerableState) {
    lastDebounceTime = millis(); // Record the time when bell state changes
    lastFlickerableState = bellState; // Update last flickerable state
  }

  // Check if debounce delay has elapsed
  if ((millis() - lastDebounceTime) > DEBOUNCE_DELAY) {
    // Handle bell ring events if not in RELAY_ON state
    if (lastSteadyState == LOW && bellState == HIGH) {
      handleBellRing(); // Call function to handle bell ring
    } else if (lastSteadyState == HIGH && bellState == LOW) {
      handleBellOff(); // Call function to handle bell off event
    }
    lastSteadyState = bellState; // Update last steady state
  }

  // Check if relay activation duration has elapsed, regardless of the current state
  if (millis() - timeSinceRelayOn >= ACTIVATION_DURATION && currentState == RELAY_ON) {
    // CLOSE LOCK
    homekit_value_t value = {.format = homekit_format_int, .int_value = 1};
    set_lock(value);
    currentState = IDLE;
  }

  // Handle state transitions and actions
  switch (currentState) {
    case WAIT_FOR_TIMEOUT:
      // Check if the timeout has occurred
      if (millis() - lastInputTime >= TIMEOUT_DURATION) {
        // If timeout occurred, reset the currentPattern
        if(validatePattern()) {
            timeSinceRelayOn = millis();
            currentState = RELAY_ON;
            currentPattern.clear();
            // OPEN LOCK
            homekit_value_t value = {.format = homekit_format_int, .int_value = 0};
            set_lock(value);
        } else {
          currentPattern.clear();
          currentState = IDLE;
        }
      }
      break;
  }
}
