#ifndef WIFI_INFO_H_
#define WIFI_INFO_H_

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#elif defined(ESP32)
#include <WiFi.h>
#endif

/**
 * @brief SSID of the WiFi network to connect to.
 */
const char *ssid = "YOUR SSID";

/**
 * @brief Password of the WiFi network.
 */
const char *password = "YOUR WIFI PASSWORD";

/**
 * @brief Connect to the WiFi network using the specified SSID and password.
 * 
 * This function sets up the WiFi connection with the provided SSID and password.
 * It waits until the connection is established and then prints the local IP address.
 */
void wifi_connect() {
  // Disable WiFi persistence and set mode to station
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoReconnect(true);

  // Attempt to connect to WiFi network
  WiFi.begin(ssid, password);
  Serial.println("WiFi connecting...");
  while (!WiFi.isConnected()) {
    delay(100);
    Serial.print(".");
  }
  Serial.print("\n");
  Serial.printf("WiFi connected, IP: %s\n", WiFi.localIP().toString().c_str());
}

#endif
