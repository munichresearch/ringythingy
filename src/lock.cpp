#include <Arduino.h>
#include "lock.hpp"
#include "main.hpp"

// Flag indicating whether the lock is currently unlocked
bool unlocked = false;

// Set up the lock by configuring pins and initializing variables
void setup_lock() {
    pinMode(PIN_RELAY_ON, OUTPUT);
    pinMode(PIN_RELAY_OFF, OUTPUT);
    pinMode(PIN_LED_UNLOCKED, OUTPUT);
    pinMode(PIN_LED_LOCKED, OUTPUT);
    unlocked = false;
}

// Open the lock mechanism
void open_lock(){
  digitalWrite(PIN_RELAY_ON, HIGH);
  delay(10);
  digitalWrite(PIN_RELAY_ON, LOW);
  digitalWrite(PIN_LED_UNLOCKED, HIGH);
  digitalWrite(PIN_LED_LOCKED, LOW);
}

// Close the lock mechanism
void close_lock(){
  digitalWrite(PIN_RELAY_OFF,HIGH);
  delay(10);
  digitalWrite(PIN_RELAY_OFF,LOW);
  digitalWrite(PIN_LED_UNLOCKED, LOW);
  digitalWrite(PIN_LED_LOCKED, HIGH);
}

// Set the lock state based on the provided value
void set_lock(const homekit_value_t value) {
  uint8_t state = value.int_value;
  cha_lock_current_state.value.int_value = state;
  unlocked = (state == 0);
  if(unlocked){
    // Lock mechanism was unsecured by iOS Home APP
    open_lock();
  }
  if(!unlocked){
    // Lock mechanism was secured by iOS Home APP
    close_lock();
  }
  
  // Report the lock mechanism current state to HomeKit
  homekit_characteristic_notify(&cha_lock_current_state, cha_lock_current_state.value);  
}