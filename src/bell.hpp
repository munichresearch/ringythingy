#ifndef BELL_H_
#define BELL_H_

// Debounce delay in milliseconds
const int DEBOUNCE_DELAY = 50;

// Duration for a short press in milliseconds
const int SHORT_PRESS_DURATION = 100;

// Duration for a long press in milliseconds
const int LONG_PRESS_DURATION = 500;

// Tolerance for detecting press duration in milliseconds
const int TOLERANCE_PRESS = 200;

// Pause between presses in milliseconds
const int PAUSE_BETWEEN_PRESSES = 100;

// Tolerance for detecting pause between presses in milliseconds
const int TOLERANCE_PAUSE = 400;

// Timeout duration for a sequence of presses in milliseconds
const int TIMEOUT_DURATION = 750;

// Duration to keep the relay activated after a valid sequence in milliseconds
const int ACTIVATION_DURATION = 1500;

// Button states
typedef enum {
  IDLE,
  WAIT_FOR_STOP,
  WAIT_FOR_TIMEOUT,
  RELAY_ON
} BellState;

void setup_bell();
void loop_input();
void detectPattern(unsigned long pressDuration);
void handleButtonPress();
void handleButtonRelease();
void printCurrentPattern();
bool validatePattern();

#endif
