<img src="logo.png" alt="RingyThingy logo" width="200"/>

# RingyThingy

RingyThingy is a smart embedded device that controls door access via WiFi or users can open the door by pressing a specific ring code. 
The device is designed to be fun, secure, and easy to use. It is compatible to Apple HomeKit.

## Features

- **WiFi Control**: Manage your door via Apple HomeKit.
- **Ring Code Access**: Open the door by pressing a predefined sequence on the bell.
- **Relay Trigger**: Activates the door relay for authorized access.

## Getting Started

### Prerequisites

- An embedded device (e.g., ESP8266/ESP32)

### Configuration

1. Open `wifi_info.h` and set your WiFi credentials:
    ```cpp
    const char *ssid = "YOUR SSID";
    const char *password = "YOUR WIFI PASSWORD";
    ```

2. Define your ring code pattern in `main.ino`:
    ```cpp
    const int pattern[] = {SHORT_RING, SHORT_RING, LONG_RING};
    ```

### Usage

- Press the bell button according to the predefined pattern to unlock the door.
- Add the device to Apple HomeKit.

## Contributing

Contributions are welcome! Please fork the repository and submit a pull request.
